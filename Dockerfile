FROM openjdk:11-jre-slim
VOLUME /tmp
ADD /target/spring-boot-store.jar spring-boot-store.jar
ENTRYPOINT ["java","-jar","/spring-boot-store.jar"]