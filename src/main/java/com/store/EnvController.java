package com.store;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@Tag(name = "Env Controller")
@RestController
@RequestMapping(value = {"", "/"}, produces = MediaType.APPLICATION_JSON_VALUE)
public class EnvController {

    private final String msg;
    private final String kuberneteMsg;

    public EnvController(@Value("${msg.value}") String msg,
                         @Value("${msg.kubernetes}") String kuberneteMsg) {
        this.msg = msg;
        this.kuberneteMsg = kuberneteMsg;
    }


    @Operation(summary = "Get env", operationId = "getEnvMsg")
    @GetMapping
    public ResponseEntity<String> getEnv() {
        return new ResponseEntity<>(msg, HttpStatus.OK);
    }

    @Operation(summary = "Get kubernetes msg", operationId = "getKubernetesMsg")
    @GetMapping("/kubernetes")
    public ResponseEntity<String> getKubernetesMsg() {
        return new ResponseEntity<>(kuberneteMsg, HttpStatus.OK);
    }
}
