package com.store;

import java.util.UUID;

public class Book {

    private String id;
    private String name;
    private String author;

    // Constructors

    public Book() {
    }

    public Book(String name, String author) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.author = author;
    }

    // Getters

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }
}
