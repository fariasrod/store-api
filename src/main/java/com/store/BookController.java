package com.store;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "*")
@Tag(name = "Books Controller")
@RestController
@RequestMapping(value = "/books", produces = MediaType.APPLICATION_JSON_VALUE)
public class BookController {

    @Operation(summary = "Get all books", operationId = "bookFindAll")
    @GetMapping
    public ResponseEntity<List<Book>> findAll() {
        return new ResponseEntity<>(books(), HttpStatus.OK);
    }


    public List<Book> books() {
        return List.of(new Book("Anna Karenina", "Leo Tolstoy"),
                new Book("Madame Bovary", "Gustave Flaubert"),
                new Book("War and Peace", "Leo Tolstoy"),
                new Book("The Great Gatsby", "F. Scott Fitzgerald"),
                new Book("Lolita", "Vladimir Nabokov"),
                new Book("Middlemarch", "George Eliot"),
                new Book("The Adventures of Huckleberry Finn", "Mark Twain"),
                new Book("The Stories of Anton Chekhov", "Anton Chekhov"),
                new Book("In Search of Lost Time", "Marcel Proust"));
    }
}
